.model small
.code
org 100h
start :
        jmp mulai
        
        Nickname    db 13,10,'Nickname Anda : $'
        Server      db 13,10,'Server Anda   : $'
        tampung_Nickname    db 30,?,30 dup(?)
        tampung_Server      db 13,?,13 dup(?)
        
        a db 01
        b db 02
        c db 03
        d db 04
        e db 05
        f db 06
        g dw 15
        
    daftar  db 13,10,'+========================================================+'
            db 13,10,'|  DAFTAR HARGA NOMINAL TOP UP DIAMOND Mobile Legend     |'
            db 13,10,'+========================================================+'
            db 13,10,'|                 Diamond Mobile Legend                  |'
            db 13,10,'+========================================================+'
            db 13,10,'|No |     NOMINAL           |            HARGA           |'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'|01 |     25 + 3 Diamond    |  Rp. 10.000                |'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'|02 |     53 + 6 Diamond    |  Rp. 20.000                |'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'|03 |     77 + 8 Diamond    |  Rp. 30.000                |'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'|04 |     154 + 16 Diamond  |  Rp. 60.000                |'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'|05 |     167 + 18 Diamond  |  Rp. 65.000                |'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'+--------------------------------------------------------+'
            db 13,10,'|06 |     200 + 20 Diamond  |  Rp. 70.000                |'
            db 13,10,'+--------------------------------------------------------+','$'
            
            
            error       db 13,10,'KODE YANG ANDA MASUKKAN TIDAK BENAR $'
            pilih_krs   db 13,10,'Silahkan Masukkan Nomor Nominal Top Up yang anda pilih : $'
            success     db 13,10,'Selamat Anda Berhasil $'
            
mulai : 
        mov ah, 09h
        lea dx,Nickname
        int 21h
        mov ah,0ah
        lea dx,tampung_Nickname 
        int 21h
        push dx
        
        mov ah,09h
        lea dx,Server
        int 21h
        mov ah,0ah
        lea dx,tampung_Server
        int 21h
        push dx
        
        mov ah,09h
        mov dx,offset daftar
        int 21h
        mov ah,09h
        jmp proses
        jne error_msg

error_msg :
            mov ah,09h
            mov dx,offset error
            int 21h
            int 20h
            
proses :
        mov ah,09h
        mov dx,offset pilih_krs
        int 21h
        
        mov ah,1
        int 21h
        mov bh,al
        mov ah,1
        int 21h
        mov bl,al
        cmp bh,'0'
        cmp bl,'1'
        je hasil1
        
        cmp bh,'0'
        cmp bl,'2'
        je hasil2
        
        cmp bh,'0'
        cmp bl,'3'
        je hasil3
        
        cmp bh,'0'
        cmp bl,'4'
        je hasil4
        
        cmp bh,'0'
        cmp bl,'5'
        je hasil5
        
        cmp bh,'0'
        cmp bl,'6'
        je hasil6
        
        jne error_msg
        
;===================================================

hasil1 :
        mov ah,09h
        lea dx,teks1
        int 21h
        int 20h
        
hasil2 :
        mov ah,09h
        lea dx,teks2
        int 21h
        int 20h
        
hasil3 :
        mov ah,09h
        lea dx,teks3
        int 21h
        int 20h
        
hasil4 :
        mov ah,09h
        lea dx,teks4
        int 21h
        int 20h
        
hasil5 :
        mov ah,09h
        lea dx,teks5
        int 21h
        int 20h
        
hasil6 :
        mov ah,09h
        lea dx,teks6
        int 21h
        int 20h
        
;========================================================

teks1   db 13,10,'Anda Memilih NOMINAL 25 + 3 Diamond '
        db 13,10,'Harga : Rp. 10.000 '
        db 13,10,'Terima Kasih $'
        
teks2   db 13,10,'Anda Memilih NOMINAL 53 + 6 Diamond '
        db 13,10,'Harga :  Rp. 20.000 '
        db 13,10,'Terima Kasih $'
        
teks3   db 13,10,'Anda Memilih NOMINAL 77 + 8 Diamond '
        db 13,10,'Harga :  Rp. 30.000 '
        db 13,10,'Terima Kasih $'
        
teks4   db 13,10,'Anda Memilih NOMINAL 154 + 16 Diamond '
        db 13,10,'Harga :  Rp. 60.000 '
        db 13,10,'Terima Kasih $'
        
teks5   db 13,10,'Anda Memilih NOMINAL 167 + 18 Diamond '
        db 13,10,'Harga :  Rp. 65.000 '
        db 13,10,'Terima Kasih $'
        
teks6   db 13,10,'Anda Memilih NOMINAL 200 + 20 Diamond '
        db 13,10,'Harga :  Rp. 70.000 '
        db 13,10,'Terima Kasih $'
        
end start
            
